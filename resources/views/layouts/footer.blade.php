 
<!--get tickets section end-->

<!--footer start -->
<footer>
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12 col-md-4">
                <div class="footer_box">
                    <!-- <div class="footer_header">
                        <h4 class="footer_title">
                            Organized By
                        </h4>
                    </div> -->
                    <div class="footer_box_body">
                        <center>
                            <a href="#">
                                <img src="{{asset('assets/img/cleander/c1.png')}}" alt="instagram">
                            </a>
                        </center>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="footer_box">
                    <div class="footer_header">
                        <h4 class="footer_title">
                            Sekretariat
                        </h4>
                    </div>
                    <div class="footer_box_body"> 
                            Jln. Taruma No 52 Medan

                    </div>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <div class="footer_box">
                    <div class="footer_header">
                        <h4 class="footer_title">
                            Kontak
                        </h4>
                    </div>
                    <div class="footer_box_body">
                        info_sekretariat@imi-sumut.or.id
                        <br>
                        +62 61 - 452 0672
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright_footer">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    &copy;<script>document.write(new Date().getFullYear());</script> | Made with <i class="fa fa-heart-o" aria-hidden="true"></i> by Bintang & Ica</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div>
                <div class="col-12 col-md-6 ">
                    <ul class="footer_menu">
                        <li>
                            <a class="nav-link " href="{{ url('contacts') }}">Kontak</a>
                        </li>
                        <li>
                            <a class="nav-link " href="{{ url('news') }}">Berita</a>
                        </li>
                        <li>
                            <a class="nav-link " href="{{ url('news1') }}">Motor</a>
                        </li>
                        <li>
                            <a class="nav-link " href="{{ url('news') }}">Mobil</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{ url('/') }}">Beranda</a>
                        </li> 
                        <!-- <li>
                            <a class="nav-link " href="{{ url('conference') }}"><font color="#4492c5"><b>Conference</b></font></a>
                        </li> -->
                        <!-- <li>
                            <a class="nav-link " href="{{ url('/'.'#events') }}">Events</a>
                        </li> -->
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>