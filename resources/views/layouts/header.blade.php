
<header class="header navbar fixed-top navbar-expand-md">
    <div class="container">
        <a class="navbar-brand logo" href="#">
            <img src="{{asset('assets/img/logo.png')}}" alt="Evento"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headernav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="lnr lnr-text-align-right"></span>
        </button>
        <div class="collapse navbar-collapse flex-sm-row-reverse" id="headernav">
            <ul class=" nav navbar-nav menu">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ url('/') }}">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('news') }}">Mobil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('news1') }}">Motor</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link " href="{{ url('generalvalue') }}">General Value</a>
                </li> -->
                <!-- <li class="nav-item">
                    <a class="nav-link " href="{{ url('conference') }}"><font color="#f8ce03"><b>Conference</b></font></a>
                </li> -->
                <!-- <li class="nav-item">
                    <a class="nav-link " href="{{ url('/'.'#events') }}">Events</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('news') }}">Berita</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ url('contacts') }}">Kontak</a>
                </li>
                <!-- <li class="search_btn">
                    <a  href="#">
                       <i class="ion-ios-search"></i>
                    </a>
                </li> -->
            </ul>
        </div>
    </div>
</header> 