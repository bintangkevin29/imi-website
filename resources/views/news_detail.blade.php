@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header_news')


<?php
    foreach ($news as $dnews) {

    }
?>
<!--events section -->
<section class="pt100 pb100">
    <div class="container">

        <div class="event_box">
            <img src="{{asset('assets/img/blog/large_'.$dnews->news_photo)}}" alt="event">
            <div class="event_info">
                <div class="event_title">
                    {{$dnews->news_title}}
                </div>
                <div class="speakers">
                    <span>{{$dnews->name}}</span>
                </div>
                <div class="event_date">
                    {{Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()}}
                </div>
            </div>
            <div class="event_word">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-12">
                       {{$dnews->news_contain}}
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
@include('layouts.footer')

<!-- jquery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!--slick carousel -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!--parallax -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!--Counter up -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!--Counter down -->
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- WOW JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>