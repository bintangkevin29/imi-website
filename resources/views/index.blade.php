@include('layouts.head')
<body>
    @include('layouts.loader')
    @include('layouts.header')
    
    <section id="home" class="home-cover">
        <div class="cover_slider owl-carousel owl-theme">
            <div class="cover_item" style="background: url('assets/img/bg/slider.png');">
             <div class="slider_content">
                <div class="slider-content-inner">
                    <div class="container">
                        <div class="slider-content-center">
                            <h2 class="cover-title">
                                Ikatan Motor Indonesia
                            </h2>
                            <strong class="cover-xl-text">IMI</strong>
                            <p class="cover-date">
                                Sumatera Utara
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cover_item" style="background: url('assets/img/bg/slider2.png');">
            <div class="slider_content">
                <div class="slider-content-inner">
                    <div class="container">
                        <div class="slider-content-center">
                            <h2 class="cover-title">
                                Ikatan Motor Indonesia
                            </h2>
                            <strong class="cover-xl-text">IMI</strong>
                            <p class="cover-date">
                                Sumatera Utara
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</section> 
<?php
foreach ($about as $dabout) { 
}
?> 
<section class="pt100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
               Mobil
           </h3>
       </div>

       <div class="row justify-content-center">
        
        <div class="col-12 col-md-8">
            <?php
            foreach ($news as $dnews) {
                ?>
                <a href="{{url('news/'.$dnews->id_news)}}">
                    <div class="blog_card">
                        <img src="{{asset('assets/img/blog/')}}{{'/'.$dnews->news_photo}}" alt="blog News ">
                        <div class="blog_box_data">
                            <span class="blog_date">
                                <?=Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()?>
                            </span>
                            <h5>
                                <?=ucwords($dnews->news_title)?><div class="blog_meta">
                                    <small>By <?=$dnews->name?></small>
                                </div>
                            </h5>
                            <p class="blog_word">
                                <?=substr($dnews->news_contain, 0, 140)?>
                            </p>
                        </div>
                    </div>
                </a>
                <?php
                break;
            }  
            ?> 
                </div> 
                <div class="col-12 col-md-4">
                    <div class="sidebar"> 
                    <div class="widget widget_latest_post">
                        <h4 class="widget-title">
                            Terbaru
                        </h4>
                        <ul>
                            <?php
                            $i = 0;
                            foreach ($news as $dnews) {
                                $i++;
                                ?>
                                <a href="{{url('news/'.$dnews->id_news)}}">
                                    <li>
                                        <div class="widget_recent_posts">
                                            <img src="{{asset('assets/img/blog/')}}{{'/thumb_'.$dnews->news_photo}}" alt="news">
                                            <div class="content">
                                                <h6><?=ucwords($dnews->news_title)?></h6>
                                                <p>by <?=$dnews->name?> / <?=Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()?></p>
                                            </div>
                                        </div>
                                    </li>
                                </a>
                                <?php
                                if($i == 6){
                                    break;
                                }
                            }
                            ?>

                        </ul>
                    </div>

                        
                    </div>
                </div>
                

            </div>
        </div>
    </section>

    <section class="pt100 bg-gray">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
               Motor
           </h3>
       </div>

       <div class="row justify-content-center">
        
        <div class="col-12 col-md-8">
            <?php
            foreach ($news2 as $dnews) {
                ?>
                <a href="{{url('news/'.$dnews->id_news)}}">
                    <div class="blog_card">
                        <img src="{{asset('assets/img/blog/')}}{{'/'.$dnews->news_photo}}" alt="blog News ">
                        <div class="blog_box_data">
                            <span class="blog_date">
                                <?=Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()?>
                            </span>
                            <h5>
                                <?=ucwords($dnews->news_title)?><div class="blog_meta">
                                    <small>By <?=$dnews->name?></small>
                                </div>
                            </h5>
                            <p class="blog_word">
                                <?=substr($dnews->news_contain, 0, 140)?>
                            </p>
                        </div>
                    </div>
                </a>
                <?php
                break;
            }  
            ?>
 
                </div> 
                <div class="col-12 col-md-4">
                    <div class="sidebar">
                        
                        
                    <div class="widget widget_latest_post">
                        <h4 class="widget-title">
                            Terbaru
                        </h4>
                        <ul>
                            <?php
                            $i = 0;
                            foreach ($news2 as $dnews) {
                                $i++;
                                ?>
                                <a href="{{url('news/'.$dnews->id_news)}}">
                                    <li>
                                        <div class="widget_recent_posts">
                                            <img src="{{asset('assets/img/blog/')}}{{'/thumb_'.$dnews->news_photo}}" alt="news">
                                            <div class="content">
                                                <h6><?=ucwords($dnews->news_title)?></h6>
                                                <p>by <?=$dnews->name?> / <?=Carbon\Carbon::parse($dnews->news_time)->toFormattedDateString()?></p>
                                            </div>
                                        </div>
                                    </li>
                                </a>
                                <?php
                                if($i == 6){
                                    break;
                                }
                            }
                            ?>

                        </ul>
                    </div>

                        
                    </div>
                </div>
                

            </div>
        </div>
    </section>
 

<!-- <section class="pb100 pt100" >
    <div class="container">
        <div class="table-responsive">
            <table class="event_calender table">
                <thead class="event_title">
                    <tr>
                        <th>
                            
                            <span>klasemen sementara</span>
                        </th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($events as $devents) {
                        $i++;
                        ?>
                        <tr>
                            <td class="event_date">
                                <?=Carbon\Carbon::parse($devents->waktu_event)->format('d')?>
                                
                            </td>
                            <td>

                                <div class="event_place">
                                    <h5><?= $devents->title?></h5> 
                                    <p><?= $devents->event_desc?></p>
                                </div>
                            </td>
                            <td>

                            </td>
                            <td class="buy_link">

                            </td>
                        </tr>
                        <?php
                    }
                    ?> 
                </tbody>
            </table>
        </div>
    </div>
</section>  -->
<section class="pt100 pb100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
                Komunitas
            </h3>
        </div>
        <div class="brand_carousel owl-carousel">
            <?php
            foreach($vip as $dvip){
                ?>
                <div class="brand_item text-center">
                    <img src="{{asset('assets/img/brands/')}}{{'/'.$dvip->img_vip}}" alt="<?=$dvip->name_vip?>">
                </div>
                <?php
            }
            ?>
            

        </div>
        <div class="col-12 mt70">
            
            <div id="map" data-lon="24.036176" data-lat=" 57.039986" class="map"></div>
            
        </div>
    </div>
</section>

@include('layouts.footer')


<script src="{{asset('assets/js/jquery.min.js')}}"></script>

<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>

<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>

<script src="{{asset('assets/js/parallax.min.js')}}"></script>

<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>

<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>

<script src="{{asset('assets/js/wow.min.js')}}"></script>

<script src="{{asset('assets/js/main.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuahgsm_qfH1F3iywCKzZNMdgsCfnjuUA"></script>
<script type="text/javascript">
    $(".countdown")
    .countdown("<?= Carbon\Carbon::parse($dabout->countdown)->toDateString()?>", function(event) {
        $(this).html(
            event.strftime('<div>%w <span>Weeks</span></div>  <div>%D <span>Days</span></div>  <div>%H<span>Hours</span></div> <div>%M<span>Minutes</span></div> <div>%S<span>Seconds</span></div>')
            );
    });
</script>
</body>
</html>