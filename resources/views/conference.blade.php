@include('layouts.head')
<body>
    @include('layouts.loader')
    @include('layouts.header')
    <?php
    foreach ($about as $dabout) {

    }

    foreach ($pricing as $dpricing) {

    }

    function thousandsCurrencyFormat($num) {

      if($num>1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('K', 'M', 'B', 'T');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

    }

    return $num;
}
?>

<!--page title section-->
<section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="inner_cover_content">
                    <h3>
                        <?=$dabout->title?> &ndash; <?=$dabout->short_title?>
                    </h3>
                </div>
            </div>
        </div>

        <div class="breadcrumbs">
            <ul>
                <li><a href="{{ url('/')}}">Home</a>   |  </li>
                <a href="{{ url('conference')}}"><span><b>Conference</b></span></a>   |   </li>
                <li><a href="{{ url('conference/speakers')}}"><small>Speakers</small></a></li>
                <li><a href="{{ url('conference/papers')}}"><small>Call for Papers</small></a></li>
                <li><a href="{{ url('conference/submission')}}"><small>Submission Guideline</small></a></li>
                <li><a href="{{ url('conference/register')}}"><small>Register</small></a></li>
                <li><a href="{{ url('conference/contacts')}}"><small>Contacts</small></a></li>
                
            </ul>
        </div>
    </div>
</section>
<!--cover section slider end -->

<!--event info -->

<section class="pt100 pb100">
    <div class="container">
        <div class="section_title">
            <h3 class="title">
                About the Conference
            </h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-12">
                <?=htmlspecialchars_decode($dabout->about)?>
            </div> 
        </div>

        <!--event features-->
        <!--event features end-->
    </div>
</section>

<section class="pb100">
    <?php
    foreach ($type as $dtype) {
        ?>
        <div class="container">
            <div class="section_title mb50">
                <h3 class="title">
                    <?=$dtype->desc?>
                </h3>
            </div>
        </div>
        <div class="row justify-content-center no-gutters pb100">
            <?php
            foreach ($committee as $dcommittee) {
                if($dcommittee->id_type == $dtype->id)
                {


                    ?>
                    <div class="col-md-3 col-sm-6">
                        <div class="speaker_box">
                            <div class="speaker_img">
                                <img src="{{asset('assets/img/speakers/')}}{{'/'.$dcommittee->speakers_img}}" alt="speaker name">
                                <div class="info_box">
                                    <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5>
                                    <p class="position">{{ $dcommittee->short_desc }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }
    ?>



</section>

<!--event count down end-->
<!--event calender-->
<section class="pb100" >
    <div class="container">
        <div class="table-responsive">
            <table class="event_calender table">
                <thead class="event_title">
                    <tr>
                        <th>
                            <i class="ion-ios-calendar-outline"></i>
                            <span>Important Dates</span>
                        </th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($events as $devents) {
                        ?>
                        <tr>
                            <td class="event_date">
                                <?=Carbon\Carbon::parse($devents->waktu_event)->format('d')?>
                                <span><?=Carbon\Carbon::parse($devents->waktu_event)->format('F')?></span>
                            </td>
                            <td>
                                <div class="event_place">
                                    <h5><?= $devents->title?></h5> 
                                    <p><?= $devents->event_desc?></p>
                                </div>
                            </td>
                            <td>

                            </td>
                            <td class="buy_link">

                            </td>
                        </tr>
                        <?php
                    }
                    ?> 
                </tbody>
            </table>
        </div>
    </div>
</section>
<section class="pt100 pb100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-calendar-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            DATE
                        </h5>
                        <p>
                            <?= Carbon\Carbon::parse($dabout->countdown)->toFormattedDateString()?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-location-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            location
                        </h5>
                        <p>
                            {{ $dabout->location }}
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-6 col-md-3  ">
                <div class="icon_box_two">
                    <i class="ion-ios-pricetag-outline"></i>
                    <div class="content">
                        <h5 class="box_title">
                            tickets
                        </h5>
                        <p>
                            IDR <?= thousandsCurrencyFormat($dpricing->price_amount)?> {{$dpricing->price_title}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--event info end -->


<!--event countdown -->
<section class="bg-img pt70 pb70" style="background-image: url('assets/img/bg/bg-img.png');">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <h4 class="mb30 text-center color-light">Counter until the big event</h4>
                <div class="countdown"></div>
            </div>
        </div>
    </div>
</section>
<!--event calender end -->

<!--brands section -->
<!-- <section class="bg-gray pt100 pb100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
                our partners
            </h3>
        </div>
        <div class="brand_carousel owl-carousel">
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b1.png')}}" alt="brand">
            </div>
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b2.png')}}" alt="brand">
            </div>

            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b3.png')}}" alt="brand">
            </div>
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b4.png')}}" alt="brand">
            </div>
            <div class="brand_item text-center">
                <img src="{{asset('assets/img/brands/b5.png')}}" alt="brand">
            </div>
        </div>
    </div>
</section> -->
<!--brands section end-->
@include('layouts.footer')
<!--footer end -->
<!-- jquery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!--slick carousel -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!--parallax -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!--Counter up -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!--Counter down -->
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- WOW JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('assets/js/main.js')}}"></script>
<script type="text/javascript">
    $(".countdown")
    .countdown("<?= Carbon\Carbon::parse($dabout->countdown)->toDateString()?>", function(event) {
        $(this).html(
            event.strftime('<div>%w <span>Weeks</span></div>  <div>%D <span>Days</span></div>  <div>%H<span>Hours</span></div> <div>%M<span>Minutes</span></div> <div>%S<span>Seconds</span></div>')
            );
    });
</script>
</body>
</html>