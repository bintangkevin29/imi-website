<?php
foreach ($about as $dabout) {

}
?>
@include('layouts.head')
<body>
    @include('layouts.loader')
    @include('layouts.header')
    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Contacts <small> &ndash; <?=$dabout->short_title?></small>

                        </h3>
                    </div>
                </div>
            </div>

            <div class="breadcrumbs">
                <ul>
                     <li><a href="{{ url('/')}}">Home</a>   |  </li>
                <a href="{{ url('conference')}}"><span>Conference</span></a>   |   </li>
                <li><a href="{{ url('conference/speakers')}}"><small>Speakers</small></a></li>
                <li><a href="{{ url('conference/papers')}}"><small>Call for Papers</small></a></li>
                <li><a href="{{ url('conference/submission')}}"><small>Submission Guideline     </small></a></li>
                <li><a href="{{ url('conference/register')}}"><small>Register</small></a></li>
                <li><a href="{{ url('conference/contacts')}}"><small><b>Contacts</b></small></a></li>
                    
                </ul>
            </div>
        </div>
    </section>
    <!--page title section end-->


    <!--contact section -->
    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-12">
                    <div class="contact_info">
                    <!-- <ul class="social_list">
                        <li>
                            <a href="#"><i class="ion-social-pinterest"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-dribbble"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="ion-social-instagram"></i></a>
                        </li>
                    </ul> -->
                    <?php
                    foreach ($data as $dt) {
                    }
                    ?>
                    <ul class="icon_list pt50">
                        <li>
                            <i class="ion-location"></i>
                            <span>
                                <?= $dt->address; ?>
                            </span>
                        </li>
                        <li>
                            <i class="ion-ios-telephone"></i>
                            <span>
                                <?= $dt->phone; ?>
                            </span>
                        </li>
                        <li>
                            <i class="ion-email-unread"></i>
                            <span>
                                <?= $dt->email; ?>
                            </span>
                        </li>

                        <li>
                            <i class="ion-planet"></i>
                            <span>
                                <?= $dt->website; ?>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="contact_form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" cols="4" rows="4" placeholder="massage"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn-rounded btn-primary">send massage</button>
                    </div>
                </div>
            </div>
            <div class="col-12 mt70">
                <!--map -->
                <div id="map" data-lon="24.036176" data-lat=" 57.039986" class="map"></div>
                <!--map end-->
            </div>
        </div>

    </div>
</section>
<!--contact section end -->



<!--get tickets section -->
@include('layouts.footer')
<!-- jquery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!--slick carousel -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!--parallax -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!--Counter up -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!--Counter down -->
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- WOW JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!--map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAuahgsm_qfH1F3iywCKzZNMdgsCfnjuUA"></script>
<!-- Custom js -->
<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>