@include('layouts.head')
<body>
    @include('layouts.loader')
    @include('layouts.header')
    <?php
    foreach ($about as $dabout) {

    }
    ?>
    <!--page title section-->
    <section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
        <div class="overlay_dark"></div>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12">
                    <div class="inner_cover_content">
                        <h3>
                            Speakers <small> &ndash; <?=$dabout->short_title?></small>
                        </h3>
                    </div>
                </div>
            </div>

            <div class="breadcrumbs">
                <ul>
                    <li><a href="{{ url('/')}}">Home</a>   |  </li>
                    <a href="{{ url('conference')}}"><span>Conference</span></a>   |   </li>
                    <li><a href="{{ url('conference/speakers')}}"><small><b>Speakers</b></small></a></li>
                    <li><a href="{{ url('conference/papers')}}"><small>Call for Papers</small></a></li>
                    <li><a href="{{ url('conference/submission')}}"><small>Submission Guideline</small></a></li>
                    <li><a href="{{ url('conference/register')}}"><small>Register</small></a></li>
                    <li><a href="{{ url('conference/contacts')}}"><small>Contacts</small></a></li>
                </ul>
            </div>
        </div>
    </section>
    <!--page title section end-->

    <!--about section -->
    <!--about section end -->

    <!--speaker section-->
    <section class="pb100 pt100">
        <?php
        foreach ($type as $dtype) {
            ?>
            <div class="container">
                <div class="section_title mb50">
                    <h3 class="title">
                        <?=$dtype->desc?>
                    </h3>
                </div>
            </div>
            <div class="row justify-content-center no-gutters pb100">
                <?php
                foreach ($committee as $dcommittee) {
                    if($dcommittee->id_type == $dtype->id)
                    {


                        ?>
                        <div class="col-md-3 col-sm-6">
                            <div class="speaker_box">
                                <div class="speaker_img">
                                    <img src="{{asset('assets/img/speakers/')}}{{'/'.$dcommittee->speakers_img}}" alt="speaker name">
                                    <div class="info_box">
                                        <h5 class="name">{{ $dcommittee->name_of_speakers }} </h5>
                                        <p class="position">{{ $dcommittee->short_desc }}</p>
                                    </div>
                                </div>
                                <div class="speaker_social">
                                    <p>
                                        <?= $dcommittee->speakers_longdesc ?>
                                    </p>

                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <?php
        }
        ?>



    </section>
    <!--speaker section end -->

    @include('layouts.footer')
    <!-- jquery -->
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('assets/js/popper.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/waypoints.min.js')}}"></script>
    <!--slick carousel -->
    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
    <!--parallax -->
    <script src="{{ asset('assets/js/parallax.min.js')}}"></script>
    <!--Counter up -->
    <script src="{{ asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!--Counter down -->
    <script src="{{ asset('assets/js/jquery.countdown.min.js')}}"></script>
    <!-- WOW JS -->
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    <!-- Custom js -->
    <script src="{{ asset('assets/js/main.js')}}"></script>
</body>
</html>