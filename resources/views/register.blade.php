@include('layouts.head')
<body>
    @include('layouts.loader')
    @include('layouts.header')
    <?php
    function thousandsCurrencyFormat($num) {

      if($num>1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('K', 'M', 'B', 'T');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

    }

    return $num;
}
?>
<?php
foreach ($about as $dabout) {

}
?>
<!--page title section-->
<section class="inner_cover parallax-window" data-parallax="scroll" data-image-src="{{asset('assets/img/bg/slider2.png')}}">
    <div class="overlay_dark"></div>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <div class="inner_cover_content">
                    <h3>
                        Registration <small> &ndash; <?=$dabout->short_title?></small>

                    </h3>
                </div>
            </div>
        </div>

        <div class="breadcrumbs">
            <ul>
                <li><a href="{{ url('/')}}">Home</a>   |  </li>
                <a href="{{ url('conference')}}"><span>Conference</span></a>   |   </li>
                <li><a href="{{ url('conference/speakers')}}"><small>Speakers</small></a></li>
                <li><a href="{{ url('conference/papers')}}"><small>Call for Papers</small></a></li>
                <li><a href="{{ url('conference/submission')}}"><small>Submission Guideline     </small></a></li>
                <li><a href="{{ url('conference/register')}}"><small><b>Register</b></small></a></li>
                <li><a href="{{ url('conference/contacts')}}"><small>Contacts</small></a></li>
            </ul>
        </div>
    </div>
</section>
<!--page title section end-->


<!--events section -->
<section class="pb100 pt100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
                Pricing table
            </h3>
        </div>

        <div class="row justify-content-center" >
            <?php
            foreach ($pricing as $dpricing) {
                ?>
                <div class="col-md-4 col-12">
                    <div class="price_box">
                        <div class="price_header">
                            <h4>
                                <?=$dpricing->price_title?>
                            </h4>
                            <h6>
                                <?=$dpricing->price_shortdesc?>
                            </h6>
                        </div>
                        <div class="price_tag" id="events">
                            <sup>IDR</sup> <?=thousandsCurrencyFormat($dpricing->price_amount)?>
                        </div>
                        <div class="price_footer">
                            <a href="#" class="btn btn-primary btn-rounded">Purchase</a>
                        </div>
                        <!-- <div class="price_features">
                            <ul>
                                <li>
                                    Early Entrance
                                </li>
                                <li>
                                    Front seat
                                </li>
                                <li>
                                    Complementary Drinks
                                </li>
                                <li>
                                    Promo Gift
                                </li>
                            </ul>
                        </div>
                        <div class="price_footer">
                            <a href="#" class="btn btn-primary btn-rounded">Purchase</a>
                        </div> -->
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</section>
<section class="pb100">
    <div class="container">
        <div class="section_title mb50">
            <h3 class="title">
                Registration Policy
            </h3>
        </div>

        <div class="row justify-content-center" >
            <div class="col-12 col-md-12">
                <?php
                foreach ($policy as $dpolicy) {
                    ?>
                    <?=$dpolicy->desc?>
                    <?php
                }
                ?>
            </div>

        </div>
        
    </div>
</section>
<!--event section end -->
@include('layouts.footer')
<!-- jquery -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/js/popper.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
<!--slick carousel -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!--parallax -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!--Counter up -->
<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
<!--Counter down -->
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- WOW JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>