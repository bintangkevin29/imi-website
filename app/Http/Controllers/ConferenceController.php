<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\lain;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function index(lain $lain)
    {
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get(); 
        $committee = DB::table('itb_speakers')
        ->select('*')
        ->get();
        $pricing = DB::table('conf_pricing')
        ->select('*')
        ->get();
        $events = DB::table('event_dates')
        ->select('*')
        ->where('display_in', '=', 3)
        ->orderBy('waktu_event', 'asc')
        ->get();
        // var_dump($events);exit;
        $type = DB::table('conf_speakers_type')
        ->select('*')
        ->get();
        foreach ($about as $dabout) {

        }

        $title = $dabout->short_title.' &ndash; Official Website';
        return view('conference', [
            'judul' => $title, 
            'about' => $about, 
            'committee' => $committee, 
            'events' => $events, 
            'pricing' => $pricing, 
            'type' => $type, 
        ]
    );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function speakers()
    {
        $committee = DB::table('itb_speakers')
        ->select('*')
        ->get();
        $type = DB::table('conf_speakers_type')
        ->select('*')
        ->get();
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get(); 
        foreach ($about as $dabout) {

        }
        $title = 'Speakers &ndash; '.$dabout->short_title;
        return view('speakers', ['judul' => $title,'committee'=>$committee,'type' => $type,'about'=>$about]);
    }

    public function papers()
    {
        $papers = DB::table('conf_topics')
        ->select('*')
        ->get();
        $events = DB::table('event_dates')
        ->select('*')
        ->where('display_in', '=', 3)
        ->orderBy('waktu_event', 'asc')
        ->get();
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get(); 
        foreach ($about as $dabout) {

        }
        $judul = 'Call for Papers &ndash; '.$dabout->short_title;
        return view('papers', compact('about','judul','papers','events'));
    }

    public function submission()
    {
        $guide = DB::table('conf_guide')
        ->select('*')
        ->get();
        $events = DB::table('event_dates')
        ->select('*')
        ->where('display_in', '=', 3)
        ->orderBy('waktu_event', 'asc')
        ->get();
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get(); 
        foreach ($about as $dabout) {

        }
        $judul = 'Call for Papers &ndash; '.$dabout->short_title;
        return view('guide', compact('about','judul','guide','events'));
    }

    public function events()
    {
        $title = 'Conference Events - 100 Years of ITB';
        return view('cevents', ['judul' => $title]);
    }

    public function news()
    {
        $news = DB::table('itb_news')
        ->join('users', function($join)
        {
            $join->on('itb_news.id_author', '=', 'users.id');
        }
    )
        ->select('*')
        ->where('display_in','=',2)
        ->orderBy('news_time', 'desc')
        ->paginate(5);
        $judul = 'Conference News - 100 Years of ITB';
        return view('cnews', compact('judul','news'));
    }

    public function contacts()
    {
        $data = DB::table('itb_contact')
        ->select('*')
        ->get();
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get(); 
        foreach ($about as $dabout) {

        }
        $judul = 'Contacts &ndash; '.$dabout->short_title;        
        return view('ccontacts', compact('judul','data','about'));    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, lain $lain)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function show(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\lain  $lain
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(lain $lain, DummyModelClass $DummyModelVariable)
    {
        //
    }
}
