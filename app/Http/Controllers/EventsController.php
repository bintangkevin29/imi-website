<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventsController extends Controller
{
    function index()
    {
    	$judul = 'Events - 100 Years of ITB';
    	return view('events', ['judul' => $judul]);
    }
}
