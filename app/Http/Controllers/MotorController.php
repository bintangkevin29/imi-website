<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class MotorController extends Controller
{
    function index()
	{
		$news = DB::table('itb_news')
		->join('users', function($join)
		{
			$join->on('itb_news.id_author', '=', 'users.id');
		}
	)
		->select('*')
		->where('display_in','=',2)
		->orderBy('news_time', 'desc')
		->paginate(5);
		$judul = 'News - 100 Years of ITB';
		return view('motorUtama', compact('judul','news'));
	} 
}
