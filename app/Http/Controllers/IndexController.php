<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 1)
        ->get(); 
        $committee = DB::table('itb_committee')
        ->select('*')
        ->get();
        $events = DB::table('event_dates')
        ->select('*')
        ->where('display_in', '=', 1)
        ->orderBy('waktu_event', 'asc')
        ->get();
        $vip = DB::table('itb_vip')
        ->select('*')
        ->get();
        $title = '100 Years of ITB - Official Website';
        $news = DB::table('itb_news')
        ->join('users', function($join)
        {
            $join->on('itb_news.id_author', '=', 'users.id');
        }
        )
        ->select('*')
        ->where('display_in','=',1)
        ->orderBy('news_time', 'desc')
        ->get();
        $news2 = DB::table('itb_news')
        ->join('users', function($join)
        {
            $join->on('itb_news.id_author', '=', 'users.id');
        }
        )
        ->select('*')
        ->where('display_in','=',2)
        ->orderBy('news_time', 'desc')
        ->get();
        return view('index', [
            'judul' => $title, 
            'about' => $about, 
            'committee' => $committee, 
            'events' => $events, 
            'vip' => $vip,
            'news' => $news,
            'news2' => $news2
        ]
    );
    }

    public function register()
    {
        $pricing = DB::table('conf_pricing')
        ->select('*')
        ->get();
        $policy = DB::table('conf_policy')
        ->select('*')
        ->get(); 
        $about = DB::table('itb_about')
        ->select('*')
        ->where('display_in', '=', 2)
        ->get(); 
        foreach ($about as $dabout) {

        }
        $judul = 'Regsitration &ndash; '.$dabout->short_title;        
        return view('register', compact('judul','pricing','about','policy'));
    }
}
