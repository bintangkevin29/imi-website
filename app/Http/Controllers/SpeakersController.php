<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpeakersController extends Controller
{
     function index()
    {
    	$committee = DB::table('itb_speakers')
		->select('*')
		->get();
    	$title = 'Speakers - 100 Years of ITB';
        return view('speakers', ['judul' => $title,'committee'=>$committee]);
    }
}
