<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/contacts', 'ContactsController@index');
Route::get('/events', 'EventsController@index');
Route::get('/news', 'NewsController@index');
Route::get('/news1', 'MotorController@index');
Route::get('/news/{id}', 'NewsController@detail');
Route::get('/conference', 'ConferenceController@index');
Route::get('/committee', 'CommitteeController@index');
Route::get('/conference/speakers', 'ConferenceController@speakers');
Route::get('/conference/papers', 'ConferenceController@papers');
Route::get('/conference/submission', 'ConferenceController@submission');
Route::get('/conference/news', 'ConferenceController@news');
Route::get('/conference/contacts', 'ConferenceController@contacts');
Route::get('/conference/register', 'IndexController@register');
