<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
    <div class="navbar navbar-default navbar-fixed-bottom footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; 2019. <a href="#" class="navbar-link"><b>ADMIN</b> &#8212; Panel</a> oleh <a href="#" class="navbar-link" target="_blank">Ica and Bintang</a>
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#">Tentang</a></li>
					<li><a href="#">Kebijakan</a></li>
					<li><a href="#">Kontak</a></li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>