@include('layouts.header')

<body class="navbar-bottom">

	<!-- Main navbar -->
	@include('layouts.navbar')
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
				<li class="active">Events</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						Pengaturan
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Conference</span> &mdash; Events</h4>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('layouts.sidebar')
			
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic responsive configuration -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Events</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
								<li><a data-action="reload"></a></li>
								<li><a data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div style="padding-left:20px;">
						<a data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Events</a>
					</div>
					<table id="tabelqu" class="table table-striped datatable-responsive">
						<thead>
							<tr>
								<th>ID</th> 
								<th>Waktu</th> 
								<th>Event</th> 
								<th>Action</th>
							</tr>
						</thead>
						<?php
						foreach ($data as $dt) {
							?>
							<tr>
								<td><?=$dt->id_events?></td>
								<td><?=Carbon\Carbon::parse($dt->waktu_event)->toFormattedDateString()?></td>
								<td><?=$dt->title?></td>
								<td>
									<a class="btn btn-primary" href="{{ url('conf/events/sunting/') }}/<?=$dt->id_events?>"><i class="icon-pencil"></i></a>&nbsp;&nbsp;
									<a class="btn btn-danger" onclick="return confirm('Events yang akan dihapus tidak dapat dikembalikan lagi. Klik OK untuk melanjutkan.');" href="{{ url('conf/events/hapus/') }}/<?=$dt->id_events?>"><i class="icon-trash"></i></a>
								</td>
							</tr>
							<?php
						}
						?>

						<tbody> 
						</tbody>
					</table>
				</div>
				<!-- /basic responsive configuration -->


				<!-- /whole row as a control -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	@include('layouts.footer')
	<!-- /footer -->

	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>

</body>
</html>

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Tambah Events</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{url('conf/events/add')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="name" class="col-md-4 control-label">Waktu</label>
						<div class="col-md-6">
							<input required id="name" autocomplete="off" type="date" class="form-control" name="date" value="">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Event</label>
						<div class="col-md-6">
							<input required id="shortdesc" autocomplete="off" type="text" class="form-control" name="event" value="">
						</div>
					</div> 
					<div class="form-group">
						<label for="email" class="col-md-4 control-label">Desc</label>
						<div class="col-md-6">
							<textarea required id="shortdesc" autocomplete="off" type="text" class="form-control" name="desc"> </textarea>
						</div>
					</div> 
					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button id="submit" type="submit" class="btn btn-primary">
								<i class="fa fa-btn fa-user"></i> Tambah
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>