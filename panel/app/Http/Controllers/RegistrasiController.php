<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;



class RegistrasiController extends Controller
{
    public function index(Request $request){
    	$method = $request->method();
    	DB::table('users')->insert(
    		[
    			'name' => $request->name,
    			'email' => $request->email,
    			'password' => password_hash($request->password, PASSWORD_DEFAULT),
    			'created_at' => date('Y-m-d H:i:s', time()),
    			'hak_akses' => $request->level
    		]);

    	 return redirect('user');
    }
}
